var express = require('express');
var router = express.Router();
var fs = require("fs");
var path = require('path');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/upload', function(req, res){
    //接收前台POST过来的base64
    var imgData = req.body.imgData;
    //过滤data:URL
    var base64Data = imgData.replace(/^data:image\/\w+;base64,/, "");
    var dataBuffer = new Buffer(base64Data, 'base64');
    var url=path.join('./', 'public')+"/out.png";

    fs.writeFile(url, dataBuffer, function(err) {
        if(err){
            res.send({meta: {code: 1, message: '请求失败'}, data:''});
        }else {
            res.send({meta: {code: 0, message: '通讯成功'}, data: {image: "out.png"}}) ;
        }
    });
});

module.exports = router;
